#!/bin/bash

for i in $(seq 10000000);
    do
        echo "a,2,c,d,5,6" | awk -v v=$i 'BEGIN {FS=OFS=","} {$2=v$2;$3=v$3;$5=v$5}{print}' >> testfile.content
done
