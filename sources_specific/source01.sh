#!/bin/bash

# Specific test based on the source/file characteristics
# Created on 20th of May 2022 by Gino Eising

## shared variables from here
INPUT_SOURCE_FILE=$1

## no shared variables from here

function empty_testresults(){

   > $INPUT_SOURCE_FILE.testresult
   rm $INPUT_SOURCE_FILE.testresult.*
}

function number_expected_lines(){
# count lines number of set lines
NUMBER_EXPECTED_LINES=6

COUNT=$(sed -n '$=' $INPUT_SOURCE_FILE)

if [[ $COUNT == $NUMBER_EXPECTED_LINES ]]; then
  echo "OK: number_expected_lines - Number of lines is expected" | tee -a $INPUT_SOURCE_FILE.testresult
else 
  echo "NOTOK: number_expected_lines - Number of lines in incorrect, this is faulty" | tee -a $INPUT_SOURCE_FILE.testresult
fi

}


function only_accepted_characters(){
# validate against set of characters

ACCEPTED_CHARACTERS=$(awk '! /[A-Za-z0-9#@]/' $INPUT_SOURCE_FILE)

# echo $ACCEPTED_CHARACTERS

if [[ $ACCEPTED_CHARACTERS == "" ]]; then
  echo "OK: only_accepted_characters - File is correct" | tee -a $INPUT_SOURCE_FILE.testresult
else 
  echo "NOTOK: only_accepted_characters - File contains non-accepted characters, this is faulty" | tee -a $INPUT_SOURCE_FILE.testresult
fi

}


function evaluate_testresults(){
# rename testresult if contains 'faulty'

if grep --quiet faulty $INPUT_SOURCE_FILE.testresult; then
  echo "testresult file is faulty"
  mv $INPUT_SOURCE_FILE.testresult $INPUT_SOURCE_FILE.testresult.FAULTY
else
  echo "testresult file is correct"
  mv $INPUT_SOURCE_FILE.testresult $INPUT_SOURCE_FILE.testresult.CORRECT
fi

}

empty_testresults
number_expected_lines
only_accepted_characters

evaluate_testresults
