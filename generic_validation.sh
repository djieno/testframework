#!/bin/bash

# Main source validation calling source specific test after correct validation
# Created on 20th or may 2022 by Gino Eising

function validate_correct_filename(){

# This function will filter all listed files with reverse grep and validate result with processing to be sure it's a unprocessed file. 

    for i in $(ls sample_sources |egrep -v '(post_generic_validation|FAULTY|testresult)')
    do
    # echo $i
        if [ ! -f "sample_sources/$i.post_generic_validation" ]; then
            echo $i
            # echo "new file, not previously processed"
                if grep -qw $i strickt_filenames; then
                    echo "OK: validate_correct_filename - $i is in strickt_filenames" | tee -a $0.log sample_sources/$i.testresult
                    mv sample_sources/$i sample_sources/$i.post_generic_validation
                else
                    echo "NOTOK: validate_correct_filename - $i is sloppy filenaming, this is FAULTY" | tee -a $0.log sample_sources/$i.testresult
                    mv sample_sources/$i.testresult sample_sources/$i.testresult.FAULTY
                    mv sample_sources/$i sample_sources/$i.FAULTY
                fi
            
        # else 
        #     echo "file is already being processed"
        fi
    done
}

function validate_source_specific(){

# This function will filter all listed files matchig post_generic_validation and remove extension.

    for i in $(ls sample_sources |egrep '(post_generic_validation)')
    do
    SOURCEFILE=${i%.post_generic_validation}
        echo sources_specific/${SOURCEFILE%.content}.sh sample_sources/$SOURCEFILE

        ( sources_specific/${SOURCEFILE%.content}.sh sample_sources/$SOURCEFILE )

        # if grep -qw $i strickt_filenames; then
        #     echo "OK: validate_correct_filename - $i is in strickt_filenames" | tee -a $0.log sample_sources/$i.testresult
        #     mv sample_sources/$i sample_sources/$i.post_generic_validation
        # else
        #     echo "NOTOK: validate_correct_filename - $i is sloppy filenaming, this is FAULTY" | tee -a $0.log sample_sources/$i.testresult
        #     mv sample_sources/$i.testresult sample_sources/$i.testresult.FAULTY
        #     mv sample_sources/$i sample_sources/$i.FAULTY
        # fi
            
        # else 
        
    done
}

# validate_correct_filename
validate_source_specific